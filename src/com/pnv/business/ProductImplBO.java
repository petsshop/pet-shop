package com.pnv.business;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.pnv.dao.ProductDAO;
import com.pnv.model.Product;

public class ProductImplBO implements ProductBO{
	ProductDAO productDAO = null;
	
	public ProductImplBO() {
		this.productDAO = new ProductDAO();
	}
	@Override
	public boolean checkSearch(String search) {
		return this.productDAO.checkSearch(search);
	}

	@Override
	public List<Product> showAllProducts() {
		try {
			return this.productDAO.getAllByProduct();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public ArrayList<Product> showListProductWithCategory(long category_id) throws SQLException {
		return this.productDAO.getListProductByCategory(category_id);
	}

	@Override
	public Product getProduct(long productID) throws SQLException {
		return this.productDAO.getProduct(productID);
	}

	@Override
	public ArrayList<Product> showListProductWithNav(long categoryID, int firstResult, int maxResult) throws SQLException {
		return this.productDAO.getListProductByNav(categoryID, firstResult, maxResult);
	}

	@Override
	public int countProductByCategory(long categoryID) throws SQLException {
		return this.productDAO.countProductByCategory(categoryID);
	}
	@Override
	public List<Product> showAllProducts(int offset, int recordPerPage) {
		try {
			return this.productDAO.getAllByProduct(offset, recordPerPage);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}
	@Override
	public int countAllProducts() throws SQLException {
		// TODO Auto-generated method stub
		return this.productDAO.countAllProduct();
	}
	
	
}
