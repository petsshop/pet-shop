package com.pnv.business;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.pnv.model.Product;

public interface ProductBO {
	public boolean checkSearch(String search);
	
	public List<Product> showAllProducts();
	
	public List<Product> showAllProducts(int offset, int recordPerPage);
	
	public ArrayList<Product> showListProductWithCategory(long category_id) throws SQLException;
	
	public Product getProduct(long productID) throws SQLException;
	
	public ArrayList<Product> showListProductWithNav(long categoryID, int firstResult, int maxResult) throws SQLException;
	
	public int countProductByCategory(long categoryID) throws SQLException;
	
	public int countAllProducts() throws SQLException;
}
