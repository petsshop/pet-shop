package com.pnv.business;

import java.sql.SQLException;

import com.pnv.dao.BillDetailDAO;
import com.pnv.model.BillDetail;

public class BillDetailImplBO implements BillDetailBO{

	BillDetailDAO billDetailDAO = null;
	
	public BillDetailImplBO() {
		this.billDetailDAO = new BillDetailDAO();
	}
	@Override
	public void addBillDetail(BillDetail bd) {
		try {
			this.billDetailDAO.insertBillDetail(bd);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
