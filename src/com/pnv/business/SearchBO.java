package com.pnv.business;

import java.util.List;

import com.pnv.model.Product;

public interface SearchBO {
	List<Product> showAllProductWithSearch();
	List<Product> showProductsWithSearchCondition(String condition);
	List<Product> showProductInRangeOfPrice(float price1, float price2);
}
