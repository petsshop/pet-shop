package com.pnv.business;

import java.sql.SQLException;
import java.util.ArrayList;

import com.pnv.dao.BillDAO;
import com.pnv.model.Bill;

public class BillImplBO implements BillBO{
	BillDAO billDAO = null;
	public BillImplBO() {
		this.billDAO = new BillDAO();
	}
	@Override
	public void addBill(Bill bill) {
		try {
			this.billDAO.insertBill(bill);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public ArrayList<Bill> showListBill() {
		// TODO Auto-generated method stub
		return this.billDAO.getListBill();
	}
	
}

