package com.pnv.business;

import java.sql.SQLException;
import java.util.ArrayList;

import com.pnv.dao.CategoryDAO;
import com.pnv.model.Category;

public class CategoryImplBO implements CategoryBO {
	CategoryDAO categoryDAO = null;
	
	public CategoryImplBO() {
		this.categoryDAO = new CategoryDAO();
	}
	@Override
	public ArrayList<Category> showListCategory() throws SQLException {
		return this.categoryDAO.getListCategory();
	}

	@Override
	public boolean addCategory(Category c) {
		return this.categoryDAO.insertCategory(c);
	}

	@Override
	public boolean updateCategory(Category c) {
		return this.categoryDAO.updateCategory(c);
	}

	@Override
	public boolean removeCategory(long category_id) {
		return this.categoryDAO.deleteCategory(category_id);
	}
	
	
}
