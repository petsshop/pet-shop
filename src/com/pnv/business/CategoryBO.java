package com.pnv.business;

import java.sql.SQLException;
import java.util.ArrayList;

import com.pnv.model.Category;

public interface CategoryBO {
	public ArrayList<Category> showListCategory() throws SQLException;
	
	public boolean addCategory(Category c);
	
	public boolean updateCategory(Category c);
	
	public boolean removeCategory(long category_id);
	
}
