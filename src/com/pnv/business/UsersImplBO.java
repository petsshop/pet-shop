package com.pnv.business;

import com.pnv.dao.UsersDAO;
import com.pnv.dao.UsersDAOImpl;
import com.pnv.model.Users;

public class UsersImplBO implements UsersBO{
	UsersDAO userDAO = null;
	
	public UsersImplBO() {
		this.userDAO = new UsersDAOImpl();
	}
	@Override
	public boolean checkEmail(String email) {
		return this.userDAO.checkEmail(email);
	}

	@Override
	public boolean insertUser(Users u) {
		return this.userDAO.insertUser(u);
	}

	@Override
	public Users login(String email, String password) {
		return this.userDAO.login(email, password);
	}

	@Override
	public Users getUser(long userID) {
		return this.userDAO.getUser(userID);
	}
	@Override
	public Users adminLogin(String email, String password, boolean userRole) {
		return this.userDAO.adminLogin(email, password, userRole);
	}
	

}
