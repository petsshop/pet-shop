package com.pnv.dao;

import com.pnv.util.DBConnect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pnv.model.Category;
import com.pnv.model.Product;

public class ProductDAO {
	private PreparedStatement ps;
	private String sql = null;
	private ResultSet rs;
	
	public boolean checkSearch(String search) {
		Connection connection = DBConnect.getConnection();
		sql = "SELECT * FROM product WHERE product_name like '%" + search + "%'";

		try {
			ps = connection.prepareCall(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				connection.close();
				return true;
			}
		} catch (SQLException ex) {
			Logger.getLogger(UsersDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
		}
		return false;
	}
	
	public List<Product> getAllByProduct() throws SQLException{
		Connection connection = DBConnect.getConnection();
		List<Product> list = new ArrayList<Product>();
		try {
			sql = "SELECT product_id, product_name, product_image, product_price, product_description FROM product";
			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery();
			
			Product sp;
			while (rs.next()) {
				sp = new Product();
				sp.setProductID(rs.getLong("product_id"));
				sp.setProductName(rs.getString("product_name"));
				sp.setProductPrice(rs.getInt("product_price"));
				sp.setProductImage(rs.getString("product_image"));
				sp.setProductDescription(rs.getString("product_description"));
				list.add(sp);
			}
		} catch (Exception e) {
			System.out.println("Sorry!!!!");
		}
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	
	public List<Product> getAllByProduct(int offset, int recordPerPage) throws SQLException{
		Connection connection = DBConnect.getConnection();
		List<Product> list= null;
		try {
			sql = "SELECT product_id, product_name, product_image, product_price, product_description FROM product limit " + offset + ", " + recordPerPage;
			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery();
			list = new ArrayList<Product>();
			Product sp;
			while (rs.next()) {
				sp = new Product();
				sp.setProductID(rs.getLong("product_id"));
				sp.setProductName(rs.getString("product_name"));
				sp.setProductPrice(rs.getInt("product_price"));
				sp.setProductImage(rs.getString("product_image"));
				sp.setProductDescription(rs.getString("product_description"));
				list.add(sp);
			}
		} catch (Exception e) {
			System.out.println("Sorry!!!!");
		}
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public ArrayList<Product> getListProductByCategory(long category_id) throws SQLException {
		Connection connection = DBConnect.getConnection();
		sql = "SELECT * FROM product WHERE category_id = '" + category_id + "'";
		ps = connection.prepareCall(sql);
		rs = ps.executeQuery();
		ArrayList<Product> list = new ArrayList<>();
		Product product = new Product();
		while (rs.next()) {
			product.setProductID(rs.getLong("product_id"));
			product.setProductName(rs.getString("product_name"));
			product.setProductImage(rs.getString("product_image"));
			product.setProductPrice(rs.getInt("product_price"));
			product.setProductDescription(rs.getString("product_description"));
			list.add(product);
		}
		return list;
	}

	public Product getProduct(long productID) throws SQLException {
		Connection connection = DBConnect.getConnection();
		sql = "SELECT * FROM product WHERE product_id = '" + productID + "'";
		ps = connection.prepareCall(sql);
		rs = ps.executeQuery();
		Product product = new Product();
		while (rs.next()) {
			product.setProductID(rs.getLong("product_id"));
			product.setProductName(rs.getString("product_name"));
			product.setProductImage(rs.getString("product_image"));
			product.setProductPrice(rs.getInt("product_price"));
			product.setProductDescription(rs.getString("product_description"));
		}
		return product;
	}

	public ArrayList<Product> getListProductByNav(long categoryID, int firstResult, int maxResult) throws SQLException {
		Connection connection = DBConnect.getConnection();
		sql = "SELECT * FROM product WHERE category_id = '" + categoryID + "' limit ?,?";
		ps = connection.prepareCall(sql);
		ps.setInt(1, firstResult);
		ps.setInt(2, maxResult);
		rs = ps.executeQuery();
		ArrayList<Product> list = new ArrayList<>();
		while (rs.next()) {
			Product product = new Product();
			product.setProductID(rs.getLong("product_id"));
			product.setProductName(rs.getString("product_name"));
			product.setProductImage(rs.getString("product_image"));
			product.setProductPrice(rs.getInt("product_price"));
			product.setProductDescription(rs.getString("product_description"));
			list.add(product);
		}
		return list;
	}

	public int countProductByCategory(long categoryID) throws SQLException {
		Connection connection = DBConnect.getConnection();
		sql = "SELECT count(product_id) FROM product WHERE category_id = '" + categoryID + "'";
		ps = connection.prepareCall(sql);
		rs = ps.executeQuery();
		int count = 0;
		while (rs.next()) {
			count = rs.getInt(1);
		}
		return count;
	}
	
	public int countAllProduct() throws SQLException{
		Connection connection = DBConnect.getConnection();
		sql = "SELECT count(product_id) FROM product";
		ps = connection.prepareCall(sql);
		rs = ps.executeQuery();
		int count = 0;
		while (rs.next()) {
			count = rs.getInt(1);
		}
		return count;
	}
	
	public boolean insertProduct(Product p) {
		Connection connection = DBConnect.getConnection();
		String sql = "INSERT INTO product VALUES(?,?,?,?,?,?)";
		try {
			PreparedStatement ps = connection.prepareCall(sql);
			ps.setLong(1, p.getProductID());
			ps.setLong(2, p.getCategoryID());
			ps.setString(3, p.getProductName());
			ps.setString(4, p.getProductImage());
			ps.setDouble(5, p.getPrice());
			ps.setString(6, p.getProductDescription());		
			return ps.executeUpdate() == 1;
		} catch (SQLException ex) {
			Logger.getLogger(CategoryDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
		return false;
	}
	

}
