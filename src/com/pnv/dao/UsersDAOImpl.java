package com.pnv.dao;

import com.pnv.util.DBConnect;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pnv.model.Users;

public class UsersDAOImpl implements UsersDAO {

	private PreparedStatement ps = null;
	private String sql = null;
	private ResultSet rs = null;

	public boolean checkEmail(String email) {
		Connection connection = DBConnect.getConnection();
		sql = "SELECT * FROM users WHERE user_email = '" + email + "'";

		try {
			ps = connection.prepareCall(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				connection.close();
				return true;
			}
		} catch (SQLException ex) {
			Logger.getLogger(UsersDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
		}
		return false;
	}

	public boolean insertUser(Users u) {
		Connection connection = DBConnect.getConnection();
		sql = "INSERT INTO users VALUES(?,?,?,?)";
		try {
			ps = connection.prepareCall(sql);
			ps.setLong(1, u.getUserID());
			ps.setString(2, u.getUserEmail());
			ps.setString(3, u.getUserPass());
			ps.setBoolean(4, u.isUserRole());
			ps.executeUpdate();
			connection.close();
			return true;
		} catch (SQLException ex) {
			Logger.getLogger(UsersDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
		}
		return false;
	}

	public Users login(String email, String password) {
		Connection connection = DBConnect.getConnection();
		sql = "select * from users where user_email='" + email + "' and user_pass='" + password + "'";

		try {
			ps = (PreparedStatement) connection.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				Users u = new Users();
				u.setUserID(rs.getLong("user_id"));
				u.setUserEmail(rs.getString("user_email"));
				u.setUserPass(rs.getString("user_pass"));
				u.setUserRole(rs.getBoolean("user_role"));
				connection.close();
				return u;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Users adminLogin(String email, String password, boolean userRole){
		Connection connection = DBConnect.getConnection();
		sql= "SELECT * FROM users WHERE user_email='" + email + "' AND user_pass='" + password + "' AND user_role= '0'";
		try {
			ps= connection.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				Users u = new Users();
				u.setUserID(rs.getLong("user_id"));
				u.setUserEmail(rs.getString("user_email"));
				u.setUserPass(rs.getString("user_pass"));
				u.setUserRole(rs.getBoolean("user_role"));
				connection.close();
				return u;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
		
	}
	public Users getUser(long userID) {
		Connection connection = DBConnect.getConnection();
		try {

			sql = "SELECT * FROM users WHERE user_id = ?";
			ps = connection.prepareCall(sql);
			ps.setLong(1, userID);
			rs = ps.executeQuery();

			Users u = new Users();

			while (rs.next()) {
				u.setUserEmail(rs.getString("user_email"));
			}
			return u;
		} catch (SQLException ex) {
			Logger.getLogger(UsersDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}

	

}
