package com.pnv.controller;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.mysql.cj.core.util.StringUtils;
import com.pnv.business.SearchBO;
import com.pnv.business.SearchImplBO;
import com.pnv.dao.ProductDAO;
import com.pnv.dao.Search;
import com.pnv.dao.SearchImpl;
import com.pnv.model.Product;
import com.pnv.model.Users;
import com.pnv.util.DBConnect;

//@WebServlet("/SearchServlet")
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private SearchBO search = new SearchImplBO();
	private String value1 = "Under 100.000";
    private String value2 = "100.000-300.000";
    private String value3 = "300.000-500.000";
    private String value4 = "More 500.000";
    private int price1 = 0;
    private int price2 = 100000;
    private int price3 = 300000;
    private int price4 = 500000;
    private int price5 = 10000000;
	public SearchServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		request.setCharacterEncoding("utf-8");
		String conditionSearch=request.getParameter("Price");

		List<Product> products = null;

		if(conditionSearch.equals(value1)){
			products = search.showProductInRangeOfPrice(price1,price2);
		}
		else if (conditionSearch.equals(value2)){
			products = search.showProductInRangeOfPrice(price2, price3);
		}
		else if (conditionSearch.equals(value3)){
			products = search.showProductInRangeOfPrice(price3, price4);
		}
		else if(conditionSearch.equals(value4)){
			products = search.showProductInRangeOfPrice(price4, price5);
		}
		request.setAttribute("products", products);
		RequestDispatcher rd = request.getRequestDispatcher("jsp/users/result.jsp");
		rd.forward(request, response);
		return;

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		request.setCharacterEncoding("utf-8");

		ProductDAO prod = new ProductDAO();
		String conditionSearch = request.getParameter("Search");
		if (StringUtils.isNullOrEmpty(conditionSearch)) {
			request.setAttribute("MS", "Sorry! You don't input data.");
			request.getRequestDispatcher("jsp/users/message.jsp").forward(request, response);
			return;
		} else if (prod.checkSearch(conditionSearch)) {
			List<Product> products = search.showProductsWithSearchCondition(conditionSearch);
			request.setAttribute("products", products);
			RequestDispatcher rd = request.getRequestDispatcher("jsp/users/result.jsp");
			rd.forward(request, response);
			return;
		} else {
			request.setAttribute("MS", "Sorry! Product not found " + "'" + conditionSearch + "'");
			request.getRequestDispatcher("jsp/users/message.jsp").forward(request, response);
			return;
		}

	}
}
