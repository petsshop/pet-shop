package com.pnv.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.pnv.business.ProductBO;
import com.pnv.business.ProductImplBO;
import com.pnv.model.Cart;
import com.pnv.model.Product;

/**
 * Servlet implementation class ProductPage
 */
// @WebServlet("/ProductPage")
public class ProductPage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ProductPage() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ProductBO productBO = new ProductImplBO();
		HttpSession session = request.getSession();
		long categoryID = 0;
		if (request.getParameter("categoryID") != null) {
			categoryID = (long) Long.parseLong(request.getParameter("categoryID"));
		}
		Cart cart = (Cart) session.getAttribute("cart");
		if (cart == null) {
			cart = new Cart();
			session.setAttribute("cart", cart);
		}
		
		int pages = 0, firstResult = 0, maxResult = 0, total = 0, maxPage=0;
		if (request.getParameter("pages") != null) {
			pages = (int) Integer.parseInt(request.getParameter("pages"));
		}

		try {
			total = productBO.countProductByCategory(categoryID);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (total <= 8) {
			firstResult = 0;
			maxResult = total;
		} else {
			firstResult = (pages - 1) * 8;
			maxResult = 8;
		}
		maxPage = (int) (total/8) +1;
		
		try {
			ArrayList<Product> listProduct = productBO.showListProductWithNav(categoryID, firstResult, maxResult);
			request.setAttribute("product", listProduct);
			request.setAttribute("total", total);
			request.setAttribute("categoryID", categoryID);
			request.setAttribute("currentPage", pages);
			request.setAttribute("maxPage", maxPage);
			RequestDispatcher rd = request.getRequestDispatcher("jsp/users/product.jsp");
		    rd.forward(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

}
