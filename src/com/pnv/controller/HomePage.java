package com.pnv.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.pnv.business.ProductBO;
import com.pnv.business.ProductImplBO;
import com.pnv.dao.ProductDAO;
import com.pnv.model.Cart;
import com.pnv.model.Product;

/**
 * Servlet implementation class HomePage
 */
// @WebServlet("/HomePage")
public class HomePage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public HomePage() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// RequestDispatcher view =
		// request.getRequestDispatcher("jsp/users/index.jsp");
		// view.forward(request, response);
		ProductBO productBO = new ProductImplBO();
		HttpSession session = request.getSession();
		Cart cart = (Cart) session.getAttribute("cart");
		if (cart == null) {
			cart = new Cart();
			session.setAttribute("cart", cart);
		}

		int page = 1;
		int recordsPerPage = 8;
		if (request.getParameter("pages") != null)
			page = Integer.parseInt(request.getParameter("pages"));

		
		int noOfRecords = 0;
		try {
			noOfRecords = (int) productBO.countAllProducts();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		int noOfPages = 0;
		if ((noOfRecords % recordsPerPage) == 0){
			noOfPages = noOfRecords/recordsPerPage ; 
		}
		else{
			noOfPages = (noOfRecords/recordsPerPage)+1;
		}
		

		try {
			List<Product> listProduct = productBO.showAllProducts((page - 1) * recordsPerPage, recordsPerPage);
			request.setAttribute("product", listProduct);
			request.setAttribute("noOfPages", noOfPages);
			request.setAttribute("currentPage", page);
			RequestDispatcher rd = request.getRequestDispatcher("jsp/users/index.jsp");
			rd.forward(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
