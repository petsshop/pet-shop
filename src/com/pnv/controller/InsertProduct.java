package com.pnv.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.pnv.business.AddProductBO;
import com.pnv.business.AddProductImplBO;
import com.pnv.business.BillBO;
import com.pnv.business.BillDetailBO;
import com.pnv.business.BillDetailImplBO;
import com.pnv.business.BillImplBO;
import com.pnv.dao.InsertDAO;
import com.pnv.model.Product;
/**
 * Servlet implementation class InsertProduct
 */
@WebServlet("/InsertProduct")
public class InsertProduct extends HttpServlet {

	private final AddProductBO addProductBO = new AddProductImplBO();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertProduct() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher view = request.getRequestDispatcher("jsp/admin/insert_product.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		Long categery = Long.parseLong(request.getParameter("categery"));
		String name = request.getParameter("name");
        String image= request.getParameter("image");
        int price = Integer.parseInt(request.getParameter("prices"));
        String description= request.getParameter("description");
		HttpSession session = request.getSession();		
		try {
			Product p = new Product();
			p.setProductID(new java.util.Date().getTime());
			p.setCategoryID(categery);
			p.setProductName(name);
			p.setProductImage("images/"+image);
			p.setProductPrice(price);
			p.setProductDescription(description);
			addProductBO.addProduct(p);
			
		} catch (Exception e) {
		}
		response.sendRedirect("/petShop/manager_product");
	}
	

}
