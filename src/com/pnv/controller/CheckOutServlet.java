/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controller;

import com.pnv.business.BillBO;
import com.pnv.business.BillDetailBO;
import com.pnv.business.BillDetailImplBO;
import com.pnv.business.BillImplBO;
import com.pnv.dao.BillDAO;
import com.pnv.dao.BillDetailDAO;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.eclipse.jdt.internal.compiler.batch.Main;

import com.pnv.model.Bill;
import com.pnv.model.BillDetail;
import com.pnv.model.Cart;
import com.pnv.model.Item;
import com.pnv.model.Users;
import com.pnv.util.SendMail;

public class CheckOutServlet extends HttpServlet {

	private final BillBO billBO = new BillImplBO();
	private final BillDetailBO billDetailBO = new BillDetailImplBO();

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher view = request.getRequestDispatcher("jsp/users/checkout.jsp");
		view.forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String payment = request.getParameter("payment");
		String address = request.getParameter("address");
		HttpSession session = request.getSession();
		Cart cart = (Cart) session.getAttribute("cart");
		Users users = (Users) session.getAttribute("user");
	
		try {
			long ID = new Date().getTime();
			Bill bill = new Bill();
			bill.setBillID(ID);
			bill.setAddress(address);
			bill.setPayment(payment);
			bill.setUserID(users.getUserID());
			bill.setDate(new Timestamp(new Date().getTime()));
			bill.setTotal(cart.totalCart());
			billBO.addBill(bill);
			for (Map.Entry<Long, Item> list : cart.getCartItems().entrySet()) {
				billDetailBO.addBillDetail(new BillDetail(0, ID, list.getValue().getProduct().getProductID(),
						list.getValue().getProduct().getPrice(), list.getValue().getQuantity()));
			}
			new SendMail().sendMail(users.getUserEmail(), "Pets Shop",
					"Hello, " + users.getUserEmail() + "\nTotal: " + cart.totalCart());
			cart = new Cart();
			session.setAttribute("cart", cart);
		} catch (Exception e) {
		}
		response.sendRedirect("/petShop/");
	}
	
}
