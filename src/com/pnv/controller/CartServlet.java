/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controller;

import com.pnv.business.ProductBO;
import com.pnv.business.ProductImplBO;
import com.pnv.dao.ProductDAO;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pnv.model.Cart;
import com.pnv.model.Item;
import com.pnv.model.Product;

public class CartServlet extends HttpServlet {

	private final ProductBO productBO = new ProductImplBO();

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		String command = request.getParameter("command");
		String productID = request.getParameter("productID");
		Cart cart = (Cart) session.getAttribute("cart");

		try {
			Long idProduct = Long.parseLong(productID);
			Product product = productBO.getProduct(idProduct);
			switch (command) {
			case "plus":
				if (cart.getCartItems().containsKey(idProduct)) {
					cart.plusToCart(idProduct, new Item(product, cart.getCartItems().get(idProduct).getQuantity()));
				} else {
					cart.plusToCart(idProduct, new Item(product, 1));
				}
				break;
			case "remove":
				cart.removeToCart(idProduct);
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("/petShop");
		}
		
		session.setAttribute("cart", cart);
		response.sendRedirect("/petShop");
	}

}
