package com.pnv.controller;

import com.pnv.business.UsersBO;
import com.pnv.business.UsersImplBO;
import com.pnv.dao.UsersDAO;
import com.pnv.dao.UsersDAOImpl;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pnv.model.Users;
import com.pnv.util.MD5;
import com.pnv.util.FormValidation;

public class UsersServlet extends HttpServlet {

	UsersBO usersBO = new UsersImplBO();

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		HttpSession session = request.getSession();
		String command = request.getParameter("command");
		String url = "";
		Users users = new Users();
		
		switch (command) {
		case "insert":
			users.setUserID(new java.util.Date().getTime());
			String user = request.getParameter("email");
			users.setUserEmail(user);
			String pass = request.getParameter("pass");
			users.setUserRole(true);
			String errorUser = new FormValidation().emailInputValidation(user);
			String errorPass = new FormValidation().passwordValidation(pass);
			if (usersBO.checkEmail(request.getParameter("email")) || !errorUser.equals("") || !errorPass.equals("")) {
				session.setAttribute("error", "Error email or password!");
				url = "/jsp/users/register.jsp";

			} else {
				String pw = MD5.encryption(pass);
				users.setUserPass(pw);
				usersBO.insertUser(users);
				session.setAttribute("user", users);
				url = "/";
			}
			break;

		case "login":
			users = usersBO.login(request.getParameter("email"), MD5.encryption(request.getParameter("pass")));
			if (users != null && users.isUserRole() == true) {
				session.setAttribute("user", users);
				url = "/";
			} else if(users != null && users.isUserRole() == false){
				session.setAttribute("user", users);
				url = "/admin";
			} else {
				session.setAttribute("error", "Error email or password!");
				url = "/jsp/users/login.jsp";
			}
			break;
		}
		
		RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
		rd.forward(request, response);

	}

}
