<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Message</title>
    </head>
    <body>
        <jsp:include page="header.jsp"></jsp:include>
        <div class="container">
		<div class="content">
			<div class="content-top">
				<div class="content-top-in">
				<center><h3>
		         <%
		            String message = (String) request.getAttribute("MS");	            
		        %>
		        <%=message %>
        		</h3></center>
        <div class="clearfix"></div>
				</div>
			</div>
			</div>
	</div>

    <jsp:include page="footer.jsp"></jsp:include>
        
    </body>
</html>
