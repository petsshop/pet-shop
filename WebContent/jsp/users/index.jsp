<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.pnv.model.Cart"%>
<%@page import="com.pnv.model.Product"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>shop</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Custom Theme files -->
<!--theme-style-->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="Bonfire Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript">
	
	
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 



</script>
<!--fonts-->
<link
	href='http://fonts.googleapis.com/css?family=Exo:100,200,300,400,500,600,700,800,900'
	rel='stylesheet' type='text/css'>
<!--//fonts-->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event) {
			event.preventDefault();
			$('html,body').animate({
				scrollTop : $(this.hash).offset().top
			}, 1000);
		});
	});
</script>
<!--slider-script-->
<script src="js/responsiveslides.min.js"></script>
<script>
	$(function() {
		$("#slider1").responsiveSlides({
			auto : true,
			speed : 500,
			namespace : "callbacks",
			pager : true,
		});
	});
</script>
<!--//slider-script-->
<script>
	$(document).ready(function(c) {
		$('.alert-close').on('click', function(c) {
			$('.message').fadeOut('slow', function(c) {
				$('.message').remove();
			});
		});
	});
</script>
<script>
	$(document).ready(function(c) {
		$('.alert-close1').on('click', function(c) {
			$('.message1').fadeOut('slow', function(c) {
				$('.message1').remove();
			});
		});
	});
</script>
</head>

<body>
	<%
		ArrayList<Product> listProduct = (ArrayList<Product>) request.getAttribute("product");
		int noOfPages = (int) request.getAttribute("noOfPages");
		int currentPage = (int) request.getAttribute("currentPage");
	%>
	<jsp:include page="header.jsp"></jsp:include>

	<jsp:include page="banner.jsp"></jsp:include>

	<div class="container">
		<div class="content">
			<div class="content-top">
				<h3 class="future">FEATURED</h3>
				<div class="content-top-in">

					<%
						for (Product p : listProduct) {
					%>

					<div class="col-md-3 md-col">
						<div class="col-md">
							<a href="single?productID=<%=p.getProductID()%>"> <img
								src="<%=p.getProductImage()%>" alt="<%=p.getProductName()%>"
								width="239px" height="207px" /></a>
							<div class="top-content">
								<h5>
									<a href="single?productID=<%=p.getProductID()%>"><%=p.getProductName()%></a>
								</h5>
								<div class="white">
									<a
										href="CartServlet?command=plus&productID=<%=p.getProductID()%>"
										class="hvr-shutter-in-vertical hvr-shutter-in-vertical2 ">ADD
										TO CART</a>
									<p class="dollar">
										<span class="in-dollar">$</span><span><%=p.getPrice()%></span>
									</p>
									<div class="clearfix"></div>
								</div>

							</div>
						</div>
					</div>
					<%
						}
					%>
					<div class="clearfix"></div>
				</div>
			</div>

			<ul class="start">
				<c:if test="${currentPage != 1}">
					<li><a href="index?pages=${currentPage-1}"><i></i></a></li>
				</c:if>
				<%
					for (int i = 1; i <= noOfPages; i++) {
				%>
				<li class="arrow"><a
					href="${pageContext.request.contextPath}/index?pages=<%=i%>"><%=i%></a></li>
				<%
					}
				%>
				<c:if test="${currentPage lt noOfPages }">
					<li><a href="${pageContext.request.contextPath}/index?pages=${currentPage+1}"><i class="next">
						</i></a></li>
				</c:if>
			</ul>
		</div>
	</div>

	<jsp:include page="footer.jsp"></jsp:include>

</body>

</html>
