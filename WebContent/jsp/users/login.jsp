<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>login</title>
</head>
<body>

	<jsp:include page="header.jsp"></jsp:include>

	<div class="container">
		<div class="account">
			<h2 class="account-in">login</h2>
			<form action="users" method="POST">
				<div>
					<p style="color: red"><%= session.getAttribute("error") %></p>
				</div>
				<div>
					<label for="email">User Name<span class="required">*</span></label><input type="email" name="email" pattern="[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*" required placeholder="Enter you Email (xxx@xxx.xxx)">
				</div>
				<div>
					<label for="pass">Password <span class="required">*</span></label> <input type="password"
						name="pass" pattern=".{3,8}" required placeholder="Enter you Password (3-8 kí tự)">
				</div>
				<p class="requiredd">* Yêu cầu bắt buộc</p>
				<input type="hidden" value="login" name="command"> <input
					type="submit" value="Login">
			</form>
		</div>
	</div>

	<jsp:include page="footer.jsp"></jsp:include>


</body>
</html>