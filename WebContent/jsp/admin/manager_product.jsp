<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.pnv.model.Product"%>
<%@page import="com.pnv.dao.ProductDAO"%>
<%@page import="java.util.List"%>
<%@page import="com.pnv.model.Product"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manager Product</title>

        <c:set var="root" value="${pageContext.request.contextPath}"/>
        <link href="${root}/css/mos-style.css" rel='stylesheet' type='text/css' />

    </head>
    <body>
	<%
            ProductDAO productDAO = new ProductDAO();
            List<Product> listProduct = productDAO.getAllByProduct();
        %>
        <jsp:include page="header.jsp"></jsp:include>

            <div id="wrapper">

            <jsp:include page="menu.jsp"></jsp:include>

                <div id="rightContent">
                    <h3>Tabel</h3>

                    <div class="informasi">
                        Đây là một thông tin dấu hiệu thông báo
                    </div>

                    <div class="gagal">
                        Đây là một dấu hiệu của các thông báo lỗi
                    </div>

                    <div class="sukses">
                        Đây là một dấu hiệu của sự thông báo thành công
                    </div>
                    <table class="data">
					
                        <tr class="data">
                            <th class="data" width="30px">ID</th>
                            <th class="data">Name</th>
                            <th class="data">Price</th>
                            <th class="data">Tùy chọn</th>
                        </tr>
                        <%
                            int count = 0;
                            for(Product p : listProduct){
                                count++;
                        %>
                        <tr class="data">
                            <td class="data" width="30px"><%=p.getProductID()%></td>
                            <td class="data"><%=p.getProductName()%></td>
                            <td class="data"><%=p.getPrice()%></td>
                            <td class="data">
                            <center>
                                <a href="#">Sửa</a>&nbsp;&nbsp; | &nbsp;&nbsp;
                           		<a href="#">Xóa</a>
                            </center>
                           		
                        </tr>
                        
					<%
						}
					%>
                    </table>
					<a href="insert_product">Insert Product</a>
                </div>
                <div class="clear"></div>

            <jsp:include page="footer.jsp"></jsp:include>

        </div>


    </body>
</html>
