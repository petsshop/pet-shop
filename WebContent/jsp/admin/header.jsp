<%@page import="com.pnv.model.Users"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>header</title>
    </head>
    <body>
	<%

		Users users = new Users();
		if (session.getAttribute("user") != null) {
			users = (Users) session.getAttribute("user");
		}
	%>
        <div id="header">
            <div class="inHeader">
                <div class="mosAdmin">
                    Hallo, <%=users.getUserEmail()%> Administrator<br>
                    <a href="#">Lihat website</a> | <a href="#">Help</a> | <a href="">Keluar</a>
                </div>
                <div class="clear"></div>
            </div>
        </div> 

    </body>
</html>
